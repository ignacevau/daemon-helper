import time
from logger import Logger
# from multiprocessing.connection import Listener
import subprocess
import os, sys, atexit
from connection import Listener, StatusCodes
import socket

import store


class DaemonHelper:

    # static fields
    client_process = None


    def __init__(self):
        self.running = False

        self.listener = Listener(
            address=("127.0.0.1", 6000), 
            accept_timeout=5, 
            receive_timeout=5)
            
        self.connection = None


    @classmethod
    def cleanup(cls):
        if cls.client_process is not None:
            cls.client_process.kill()


    def run(self):
        atexit.register(DaemonHelper.cleanup)

        try:
            self.connect()
            self.running = True
        except Exception as e:
            Logger.error("Daemon client couldn't be started: %s" % e)

        # start the receive loop to receive data from the client
        self.receive_loop()


    def connect(self):
        while(self.connection is None):
            # check if daemon-client is responding
            try:
                self.connection = self.listener.accept()
                Logger.log("Connection accepted")
            # timeout, daemon-client is not running
            except (TimeoutError, socket.timeout) as e:
                Logger.log("Connection timeout while connecting to client")
                # start daemon-client
                try:
                    base_path = os.path.abspath("../../daemon-client/source/")
                    python_path = os.path.abspath("../../daemon-client/source/virt/Scripts/python.exe")
                    client_path = os.path.abspath("../../daemon-client/source/main.py")

                    Logger.log("Restarting client...")
                    self.client_process = subprocess.Popen([python_path, client_path], cwd=base_path, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
                # couldn't start daemon-client
                except Exception as e:
                    # start daemon-client recovery
                    Logger.error("Couldn't start daemon client: %s" % e)
                    Logger.warn("Starting daemon client recovery...: %s" % e)

                    # TODO RECOVERY
                    # DONT OVERWRITE YOUR OWN FILES!

            # connecting failed
            except Exception as e:
                Logger.error("Couldn't connect to client: %s" % e)
                self.stop()


    def receive_loop(self):
        while self.running:
            # receive message from client
            try:
                client_status:StatusCodes = self.connection.recv()
                if client_status == StatusCodes.HEALTHY:
                    pass
                elif client_status == StatusCodes.CRITICAL:
                    Logger.log("Client status is critical, restarting...")
                    self.stop()
                else:
                    Logger.error("Unknown response from client")
                    self.stop()
            # timeout
            except TimeoutError:
                Logger.log("Client didn't respond, restarting...")
                self.stop()
            # receiving message failed
            except Exception as e:
                Logger.error("Something went wrong while receiving data from client: %s" % e)
                self.stop()

            # wait for delay until checking client again
            time.sleep(store.RECEIVE_DELAY)
    

    def stop(self):
        # kill client
        DaemonHelper.cleanup()
        # close connection
        if self.connection is not None:
            self.connection.close()
        # close listener
        if self.listener is not None:
            self.listener.close()

        self.running = False

        # close the program, it will auto-restart
        sys.exit()


if __name__ == "__main__":
    helper = DaemonHelper()
    helper.run()