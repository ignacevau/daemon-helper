from enum import Enum
import json
import uuid

import store


class Logger:
    class LogType(Enum):
        LOG=0
        WARN=1
        ERROR=2

    # logger singleton pattern
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(Logger, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        pass


    @staticmethod
    def log(text:str):
        print(("%s: " % "LOG") + text)

        
    @staticmethod
    def warn(text:str):
        print(("%s: " % "WARN") + text)

    
    @staticmethod
    def error(text:str):
        print(("%s: " % "ERROR") + text)
Logger()